class Rover
  attr_reader :x, :y, :degrees

  def initialize(gridWidth, gridHeight, x, y, direction)
    @gridWidth = gridWidth
    @gridHeight = gridHeight
    @x = x
    @y = y
    @degrees = Rover.direction_to_degrees(direction)
  end

  def self.direction_to_degrees(direction)
    {"N" => 0, "E" => 90, "S" => 180, "W" => 270}[direction]
  end

  def self.degrees_to_direction(degrees)
    case (degrees / 90) % 4
    when 0
      "N"
    when 1
      "E"
    when 2
      "S"
    when 3
      "W"
    end
  end

  def instruct(instructions)
    instructions.chars.each do |char|
      instruct_single(char)
    end
  end

  def instruct_single(instruction)
    if instruction == "L"
      rotate_left
    elsif instruction == "R"
      rotate_right
    elsif instruction == "M"
      move_forward
    end
  end

  def rotate_left
    @degrees = @degrees - 90
  end

  def rotate_right
    @degrees = @degrees + 90
  end

  def move_forward
    direction = Rover.degrees_to_direction(degrees)
    if direction == "N"
      @y = @y + 1 unless @y + 1 > @gridHeight
    elsif direction == "E"
      @x = @x + 1 unless @x + 1 > @gridWidth
    elsif direction == "S"
      @y = @y - 1 unless @y - 1 < 0
    elsif direction == "W"
      @x = @x - 1 unless @x - 1 < 0
    end
  end

  def print_location
    puts "#{x} #{y} #{Rover.degrees_to_direction(degrees)}"
  end
end
