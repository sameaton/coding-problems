require "./rover"

grid = { "width" => 5, "height" => 5 }

rover1 = Rover.new grid["width"], grid["height"], 1, 2, "N"
rover1.instruct "LMLMLMLMM"
rover1.print_location

rover2 = Rover.new grid["width"], grid["height"], 3, 3, "E"
rover2.instruct "MMRMMRMRRM"
rover2.print_location
