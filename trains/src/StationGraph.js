import _ from "lodash"

/*Generates a graph of stations given a list.
*
* Example Input:
*  ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
*
* Example Output:
*  {
*    "A": {
*      "B": 5,
*      "D": 5,
*      "E": 7
*    },
*    "B": {
*      "C": 4
*    },
*    "C": {
*      "D": 8,
*      "E": 2
*    },
*    "D": {
*      "C": 8,
*      "E": 6
*    },
*    "E": {
*      "B": 3
*    }
*  }
*
*/
export function generateStationGraph(stationsList) {
  const stationsGraphResult = {}

  stationsList.forEach(stations => {
    const fromStation = stations[0]
    const toStation = stations[1]
    const distance = parseInt(stations[2])

    if (!stationsGraphResult[fromStation])
      stationsGraphResult[fromStation] = {}

    stationsGraphResult[fromStation][toStation] = distance
  })

  return stationsGraphResult
}

export default class StationGraph {
  constructor(stationsList) {
    this.stationsGraph = generateStationGraph(stationsList)
  }

  /*Finds all of the paths from one station to another and the associated distances
  */
  findAllPaths(current, dest, visited = [], accumDistance = 0) {
    visited.push(current)

    const results = []

    _.forOwn(this.stationsGraph[current], (distance, location) => {
      // arrays are passed by reference so the visted state is cloned
      const visitedCloned = visited.map(x => x)

      // if the location is the destination, we are at the end of the route
      if (location === dest) {
        visitedCloned.push(dest)
        results.push({ path: visitedCloned, distance: accumDistance + distance })
      }
      // if we have already visited the location, we skip this loop
      else if (_.includes(visitedCloned, location))
        return
      // otherwise we recursively call this function with the next location
      else {
        const result = this.findAllPaths(
          location, dest, visitedCloned, accumDistance + distance
        )
        if (result)
          results.push(result)
      }
    })

    return _.flatten(results)
  }

  /*Finds the path with the shortest distance
  */
  findShortestPath(source, dest) {
    if (!source || typeof source !== "string")
      throw new Error("findPathsWithMaxStopCount `source` must be a string")
    if (!dest || typeof dest !== "string")
      throw new Error("findPathsWithMaxStopCount `dest` must be a string")

    const results = this.findAllPaths(source, dest, [], 0)
    if (results.length)
      return _.minBy(results, ({ path, distance }) => distance)
    else
      return []
  }

  /*Finds all the paths from one station to another within a max number of stops
  */
  findPathsWithMaxStopCount(source, dest, maxStops) {
    if (!source || typeof source !== "string")
      throw new Error("findPathsWithMaxStopCount `source` must be a string")
    if (!dest || typeof dest !== "string")
      throw new Error("findPathsWithMaxStopCount `dest` must be a string")
    if (
      !maxStops ||
      typeof maxStops !== "number" ||
      parseInt(maxStops) !== maxStops ||
      maxStops <= 0
    )
      throw new Error("findPathsWithMaxStopCount `stopCount` must be a positive integer")

    const results = this.findAllPaths(source, dest, [], 0)
    if (results.length)
      return results.filter(resultObj => resultObj.path.length <= maxStops + 1)
    else
      return []
  }

  /*Finds all the paths from one station to another with an exact number of
  * stops in between
  */
  findPathsWithExactStopCount(source, dest, stopCount) {
    if (!source || typeof source !== "string")
      throw new Error("findPathsWithExactStopCount `source` must be a string")
    if (!dest || typeof dest !== "string")
      throw new Error("findPathsWithExactStopCount `dest` must be a string")
    if (
      !stopCount ||
      typeof stopCount !== "number" ||
      parseInt(stopCount) !== stopCount ||
      stopCount <= 0
    )
      throw new Error("findPathsWithExactStopCount `stopCount` must be a positive integer")

    const results = this.findAllPaths(source, dest, [], 0)
    if (results.length)
      return results.filter(resultObj => resultObj.path.length === stopCount + 1)
    else
      return []
  }

  /*Calculates the distance through a list of stations
  */
  getFixedRouteDistance(locations) {
    if (!_.isArray(locations))
      throw new Error("getFixedRouteDistance expects an array")

    const notFoundMessage = "PATH NOT FOUND"
    let total = 0

    for (let i = 0; i < locations.length; i++) {
      if (i === locations.length - 1)
        return total || notFoundMessage
      const current = locations[i]
      const next = locations[i + 1]

      if (this.stationsGraph[current] && this.stationsGraph[current][next])
        total += this.stationsGraph[current][next]
      else
        return notFoundMessage
    }
  }
}
