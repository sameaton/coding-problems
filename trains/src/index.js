import StationGraph from "./StationGraph"

const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
const stationGraph = new StationGraph(stations)

console.log("Output #1:", stationGraph.getFixedRouteDistance(["A", "B", "C"]))
console.log("Output #2:", stationGraph.getFixedRouteDistance(["A", "D"]))
console.log("Output #3:", stationGraph.getFixedRouteDistance(["A", "D", "C"]))
console.log("Output #4:", stationGraph.getFixedRouteDistance(["A", "E", "B", "C", "D"]))
console.log("Output #5:", stationGraph.getFixedRouteDistance(["A", "E", "D"]))
console.log("Output #6:", stationGraph.findPathsWithMaxStopCount("C", "C", 3).length)
console.log("Output #7:", stationGraph.findPathsWithExactStopCount("A", "C", 4).length)
console.log("Output #8: ", stationGraph.findShortestPath("A", "C"))
console.log("Output #9: ", stationGraph.findShortestPath("B", "B"))
console.log("Output #10: ", stationGraph.findAllPaths("C", "C"))
