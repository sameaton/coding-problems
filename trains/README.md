# Train Router

Utility functions for path discovery and calutlation

## Assumptions and Changes

The instructions had some and example problems had some contradictions, so the algorithms provided by this library are slightly different.

The instructions stated that "a given route will never appear more than once" and "the starting and ending town will not be the same town", yet problems #6, #9, and #10 contradicted these instructions.

This library enforces the rule that a given route can never appear more than once unless it is the first and last location in a path.

This is not valid: `CEBCEBC`

This is valid: `CDEBC`

## Development

Later instructions assume you have node instaled and have installed the project dependencies by running:

```
npm install
```

### Running Tests

To run tests, run:

```
npm run test
```

### Running

To run the problems, do:

```
npm run start
```

This will automatically transpile the code before running.

## Documentation

All interactions with the train router are done through the `StationGraph` class. Initialize a `StationGraph` class by passing an array of stations and their distances.

*Example:*

```javascript
import StationGraph from "./StationGraph"

const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
const stationGraph = new StationGraph(stations)
```

### Methods

The following are the methods for a `StationGraph` instance:

#### `getFixedRouteDistance`

Visits each station in the list and accumulates the distance if a contiguous path exists.

##### Definition

```javascript
getFixedRouteDistance(locations)
```

##### Arguments

- `locations` : `Array(String)` - An array of strings where each string is a train station

##### Example

```javascript
const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7", "BD6"]
const stationGraph = new StationGraph(stations)
stationGraph.getFixedRouteDistance(["A", "B", "D", "C", "E"]) // 21
```

#### `findAllPaths`

Finds all of the valid paths and associated distances bewteen two stations.

##### Definition

```javascript
findAllPaths(startingStation, endingStation)
```

##### Arguments

- `startingStation` : `String` - The name of the starting station
- `endingStation` : `String` - The name of the ending station

##### Example

```javascript
const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
const stationGraph = new StationGraph(stations)
stationGraph.findAllPaths("A", "B")
/*
  [
    { path: [ 'A', 'B' ],                distance: 5  },
    { path: [ 'A', 'D', 'C', 'E', 'B' ], distance: 18 },
    { path: [ 'A', 'D', 'E', 'B' ],      distance: 14 },
    { path: [ 'A', 'E', 'B' ],           distance: 10 }
  ]
*/
```

#### `findShortestPath`

Finds the path with the shortest distance bewteen two stations.

##### Definition

```javascript
findShortestPath(startingStation, endingStation)
```

##### Arguments

- `startingStation` : `String` - The name of the starting station
- `endingStation` : `String` - The name of the ending station

##### Example

```javascript
const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
const stationGraph = new StationGraph(stations)
stationGraph.findAllPaths("A", "C")
// { path: [ 'A', 'B', 'C' ], distance: 9 }
```

#### `findPathsWithMaxStopCount`

Finds the paths don't have more than given number of stops.

##### Definition

```javascript
findPathsWithMaxStopCount(startingStation, endingStation, maxStopCount)
```

##### Arguments

- `startingStation` : `String` - The name of the starting station
- `endingStation` : `String` - The name of the ending station
- `maxStopCount` : `Integer` - The max number of stops

##### Example

```javascript
const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
const stationGraph = new StationGraph(stations)
stationGraph.findPathsWithMaxStopCount("C", "C", 3)
/*
  [
    { path: [ 'C', 'D', 'C' ], distance: 16 },
    { path: [ 'C', 'E', 'B', 'C' ], distance: 9 }
  ]
*/
```

#### `findPathsWithExactStopCount`

Finds the paths don't have more than given number of stops.

##### Definition

```javascript
findPathsWithExactStopCount(startingStation, endingStation, stopCount)
```

##### Arguments

- `startingStation` : `String` - The name of the starting station
- `endingStation` : `String` - The name of the ending station
- `stopCount` : `Integer` - The number of stops

##### Example

```javascript
const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
const stationGraph = new StationGraph(stations)
stationGraph.findPathsWithExactStopCount("A", "C", 4)
/*
  [
    { path: [ 'A', 'D', 'E', 'B', 'C' ], distance: 18 }
  ]
*/
```
