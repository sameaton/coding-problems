const StationGraph = require("../dist/StationGraph").default
const expect = require("chai").expect

describe("StationGraph", () => {
  describe("findAllPaths", () => {
    const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"]
    const stationGraph = new StationGraph(stations)

    it("finds all paths from one station to another", () => {
      const result = stationGraph.findAllPaths("A", "B")
      expect(result.length).to.eql(4)
      expect(result[0].path).to.eql(["A", "B"])
      expect(result[1].path).to.eql(["A", "D", "C", "E", "B"])
      expect(result[2].path).to.eql(["A", "D", "E", "B"])
      expect(result[3].path).to.eql(["A", "E", "B"])
    })
    it("returns an empty array if start and end is the same", () => {
      const result = stationGraph.findAllPaths("A", "A")
      expect(result.length).to.eql(0)
    })
  })

  describe("findShortestPath", () => {
    const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7", "BD6"]
    const stationGraph = new StationGraph(stations)

    it("finds the shortest path from one station to another", () => {
      expect(stationGraph.findAllPaths("E", "C").length).to.eql(2)

      const result = stationGraph.findShortestPath("E", "C")
      expect(result.path).to.eql(["E", "B", "C"])
      expect(result.distance).to.eql(7)
    })
    it("returns an empty array if a path is not found", () => {
      const result = stationGraph.findShortestPath("E", "A")
      expect(result).to.eql([])
    })
    it("throws an error if given invalid arguments", () => {
      expect(() => stationGraph.findShortestPath("A", true)).to.throw()
      expect(() => stationGraph.findShortestPath()).to.throw()
      expect(() => stationGraph.findShortestPath(null, "B")).to.throw()
      expect(() => stationGraph.findShortestPath(1, "B")).to.throw()
    })
  })

  describe("findPathsWithMaxStopCount", () => {
    const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7", "BD6"]
    const stationGraph = new StationGraph(stations)

    it("finds the paths within a max stop count", () => {
      expect(stationGraph.findAllPaths("E", "C").length).to.eql(2)

      const result = stationGraph.findPathsWithMaxStopCount("E", "C", 2)
      expect(result.length).to.eql(1)
      expect(result[0].path).to.eql(["E", "B", "C"])
      expect(result[0].distance).to.eql(7)
    })
    it("returns an empty array if a path is not found", () => {
      const result = stationGraph.findPathsWithMaxStopCount("E", "A", 2)
      expect(result).to.eql([])
    })
    it("throws an error if given invalid arguments", () => {
      expect(() => stationGraph.findPathsWithMaxStopCount("A", "B")).to.throw()
      expect(() => stationGraph.findPathsWithMaxStopCount()).to.throw()
      expect(() => stationGraph.findPathsWithMaxStopCount("A", "B", -1)).to.throw()
      expect(() => stationGraph.findPathsWithMaxStopCount("A", "B", 1.1)).to.throw()
      expect(() => stationGraph.findPathsWithMaxStopCount("A", "B", 0)).to.throw()
    })
  })

  describe("findPathsWithExactStopCount", () => {
    const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7", "BD6"]
    const stationGraph = new StationGraph(stations)

    it("finds the paths with an exact number of stops", () => {
      expect(stationGraph.findAllPaths("E", "C").length).to.eql(2)

      const result = stationGraph.findPathsWithExactStopCount("E", "C", 3)
      expect(result.length).to.eql(1)
      expect(result[0].path).to.eql(["E", "B", "D", "C"])
      expect(result[0].distance).to.eql(17)

      // if num of stops is incremented, it returns nothing
      expect(stationGraph.findPathsWithExactStopCount("E", "C", 4).length).to.eql(0)
    })
    it("returns an empty array if a path is not found", () => {
      const result = stationGraph.findShortestPath("E", "A")
      expect(result).to.eql([])
    })
    it("throws an error if given invalid arguments", () => {
      expect(() => stationGraph.findPathsWithExactStopCount("A", "B")).to.throw()
      expect(() => stationGraph.findPathsWithExactStopCount()).to.throw()
      expect(() => stationGraph.findPathsWithExactStopCount("A", "B", -1)).to.throw()
      expect(() => stationGraph.findPathsWithExactStopCount("A", "B", 1.1)).to.throw()
      expect(() => stationGraph.findPathsWithExactStopCount("A", "B", 0)).to.throw()
    })
  })

  describe("getFixedRouteDistance", () => {
    const stations = ["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7", "BD6"]
    const stationGraph = new StationGraph(stations)

    it("finds the paths with an exact number of stops", () => {
      const result = stationGraph.getFixedRouteDistance(["A", "B", "D", "C", "E"])
      expect(result).to.eql(21)
    })
    it("returns message if a path is not found", () => {
      const result = stationGraph.getFixedRouteDistance(["A", "B", "W", "C"])
      expect(result).to.eql("PATH NOT FOUND")
    })
    it("throws an error if given a non-array", () => {
      expect(() => stationGraph.getFixedRouteDistance("A", "B")).to.throw()
      expect(() => stationGraph.getFixedRouteDistance()).to.throw()
    })
  })
})

