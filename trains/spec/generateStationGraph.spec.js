const StationGraph = require("../dist/StationGraph")
const expect = require("chai").expect

describe("generateStationGraph", () => {
  it("generates a valid station graph", () => {
    const input = [
      "AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7", "BD6"
    ]
    expect(typeof StationGraph.generateStationGraph).to.eql("function")

    const stationGraphMap = StationGraph.generateStationGraph(input)
    const expectedResult = {
      A: { B: 5, D: 5, E: 7 },
      B: { C: 4, D: 6 },
      C: { D: 8, E: 2 },
      D: { C: 8, E: 6 },
      E: { B: 3 }
    }

    expect(Object.keys(stationGraphMap).length).to.eql(5)
    expect(stationGraphMap).to.eql(expectedResult)
  })
})

